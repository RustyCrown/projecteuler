from time import time

def main(nbDigits):
    largestPalindrome = 0

    for i in range(10**nbDigits):
        for j in range(10**nbDigits - 1, -1, -1):
            result = i * j

            s = str(result)
            if s == s[::-1]:
                largestPalindrome = max(largestPalindrome, result)
                break

    print(largestPalindrome)


if __name__ == '__main__':
    nbDigits = 4

    t1 = time()
    main(nbDigits)
    t2 = time()

    print('Time used by main():', round(t2 - t1, 3), 'seconds.')