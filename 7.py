from time import time
from math import ceil, sqrt


def isPrime(number):
    prime = True

    for dividend in range(2, ceil(sqrt(number)) + 1):
        if number % dividend == 0:
            prime = False
            return prime
    print('hello)

    return prime


def main(n):
    index = 2
    number = 3

    while index < n:
        number += 2
        if isPrime(number):
            index += 1
            print(index, number)

    print(number)


if __name__ == '__main__':
    n = 10001

    t1 = time()

    main(n)

    t2 = time()

    print('Time used by main():', round(t2 - t1, 3), 'seconds.')
