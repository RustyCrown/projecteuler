from time import time

def main(number):
    factor = 2
    while number % factor == 0:
        number = number // factor

    factor = 3  
    while number != 1:
        if number % factor == 0:
            number = number // factor
        else:
            factor += 2

    print(factor)

if __name__ == '__main__':
    number = 600851475143

    t1 = time()
    main(number)
    t2 = time()

    print('TIme used by main():', round(t2 - t1, 3), 'seconds.')