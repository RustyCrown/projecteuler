from time import time
from math import ceil, sqrt


def isPrime(number):
    prime = True

    for dividend in range(2, ceil(sqrt(number))):
        if number % dividend == 0:
            prime = False
            return prime

    return prime


def main(UB):
    step = 1

    for i in range(1, 1 + UB):
        if isPrime(i):
            step *= i

    number = step
    while True:
        isEvenlyDivisible = True
        for i in range(1, 1 + UB):
            if number % i != 0:
                isEvenlyDivisible = False
                break

        if isEvenlyDivisible:
            break
        else:
            number += step

    print(number)


if __name__ == '__main__':
    UB = 20

    t1 = time()
    main(UB)
    t2 = time()

    print('Time used by main():', round(t2 - t1, 3), 'seconds.')
