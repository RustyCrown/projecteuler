from time import time

# TODO:
# NOTE:
# WARN:
# HACK:
# PERFORMANCE:
#
# @todo
# @note
# @warn
# @hack
# @performance


def main(UB):
    sum = 0

    for i in range((UB - 1) // 3):
        sum += 3 * (i + 1)

    for i in range((UB - 1) // 5):
        sum += 5 * (i + 1)

    for i in range((UB - 1) // 15):
        sum -= 15 * i


def main2(UB):
    print(sum([i for i in range(0, UB) if i % 3 == 0 or i % 5 == 0]))


if __name__ == '__main__':
    UB = 100000000

    t1 = time()
    main(UB)
    t2 = time()
    main2(UB)
    t3 = time()

    print('Time used by main():', round(t2 - t1, 3), 'seconds.')
    print('Time used by main2():', round(t3 - t2, 3), 'seconds.')
