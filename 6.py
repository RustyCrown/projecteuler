from time import time


def main(n):
    sumOfSquares = 0
    squareOfSum = 0

    for i in range(1, n + 1):
        sumOfSquares += i * i
        squareOfSum += i

    squareOfSum *= squareOfSum

    print('The difference between the sum of the squares of the first', n, 'natural numbers and the square of the sum is:', squareOfSum - sumOfSquares)


if __name__ == '__main__':
    n = 100

    t1 = time()
    main(n)
    t2 = time()

    print('Time used by main():', round(t2 - t1, 3), 'seconds.')
