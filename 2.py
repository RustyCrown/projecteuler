from time import time

def main(UB):
    um2 = 1
    um1 = 2
    u = um2 + um1

    s = 2

    while u <= UB:
        um2 = um1
        um1 = u
        u = um2 + um1
    
        if u % 2 == 0:
            s += u

    print(s)

if __name__ == '__main__':
    UB = 4e6
    t1 = time()
    main(UB)
    t2 = time()
    
    print('Time used by main():', round(t2 - t1, 3), 'seconds.')